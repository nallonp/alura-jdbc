package com.alura.jdbc;

import java.sql.Connection;

public class TestaDataSource {
    public static void main(String[] args) {
        try (Connection connection = DataSource.getConnection()) {
            System.out.println("conexão adquirida");
        } catch (Exception e) {
            System.out.println("falha na conexão!");
            System.out.println(e.getMessage());
        }
    }
}
