package com.alura.jdbc.modelo;

import com.alura.jdbc.ConnectionFactory;
import com.alura.jdbc.dao.ProdutoDAO;

import java.sql.Connection;
import java.sql.SQLException;

public class TestaInsercaoEListagemComProduto {
    public static void main(String[] args) throws SQLException {
        Produto produto = new Produto("Cômoda", "Cômoda Vertical");
        try (Connection connection = ConnectionFactory.getConnection()) {
            ProdutoDAO produtoDAO = new ProdutoDAO(connection);
            produtoDAO.salvarProduto(produto);
            produtoDAO.listar().forEach(p -> System.out.println(p.toString()));
        }
    }
}
