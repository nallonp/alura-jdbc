package com.alura.jdbc.modelo;

import com.alura.jdbc.ConnectionFactory;
import com.alura.jdbc.dao.CategoriaDAO;

import java.sql.Connection;
import java.sql.SQLException;

public class TestaListagemDeCategorias {
    public static void main(String[] args) throws SQLException {
        try (Connection connection = ConnectionFactory.getConnection()) {
            CategoriaDAO categoriaDAO = new CategoriaDAO(connection);
            categoriaDAO.listarComProdutos().forEach(c -> {
                System.out.println(c.toString());
                for (Produto produto : c.getProdutos()) System.out.println(c.getNome() + " - " + produto.getNome());
            });
        }
    }
}
