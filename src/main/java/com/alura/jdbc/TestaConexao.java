package com.alura.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class TestaConexao {

    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost/loja_virtual";
        Properties props = new Properties();
        props.setProperty("user", "nallon");
        props.setProperty("password", "Fritação700");
        props.setProperty("useTimezone", "true");
        props.setProperty("serverTimezone", "UTC");
        try (Connection con = DriverManager.getConnection(url, props)) {
            System.out.println("Conexão aberta!");
        } catch (SQLException throwables) {
            System.out.println("Uma exceção foi lançada: falha na abertura da conexão!");
            System.out.println("Erro: " + throwables.getMessage());
        }
    }
}
