package com.alura.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory {
    public static Connection getConnection() throws SQLException {
        return DataSource.getConnection();
    }
}
