package com.alura.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercaoComParametro {
    public static void main(String[] args) throws SQLException {
        try (Connection con = ConnectionFactory.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement("INSERT INTO loja_virtual.produto (nome, descricao) VALUES ( ? , ?)",
                    Statement.RETURN_GENERATED_KEYS)
            ) {
                con.setAutoCommit(false);
                adicionarVariavel("SmartTV", "45 polegadas", statement);
                adicionarVariavel("Radio", "Radio de Bateria", statement);
                con.commit();
            } catch (SQLException e) {
                System.out.println("Uma exceção foi lançada: ");
                System.out.println("Erro: " + e.getMessage());
                System.out.println("Rollback executado com sucesso.");
                con.rollback();
                throw new SQLException(e);
            }
        }
    }

    private static void adicionarVariavel(String nome, String descricao, PreparedStatement statement) throws SQLException {
        // if (nome.equals("Radio")) throw new SQLException("Exceção lançada de propósito!");
        statement.setString(1, nome);
        statement.setString(2, descricao);
        statement.execute();
        System.out.print("Produtos inserido: ");
        try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
            while (generatedKeys.next()) {
                Integer id = generatedKeys.getInt(1);
                System.out.println(id);
            }
        }
    }
}
