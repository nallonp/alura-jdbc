package com.alura.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestaListagem {
    public static void main(String[] args) {
        try (Connection con = ConnectionFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("SELECT id, nome, descricao FROM loja_virtual.produto")
        ) {
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                while (resultSet.next()) {
                    Integer id = resultSet.getInt("id");
                    String nome = resultSet.getString("nome");
                    String descricao = resultSet.getString("descricao");
                    System.out.println(id + "\t" + nome + "\t" + descricao);
                }
            }
        } catch (SQLException e) {
            System.out.println("Uma exceção foi lançada: falha na abertura da conexão! ");
            System.out.println("Erro: " + e.getMessage());
        }
    }
}
