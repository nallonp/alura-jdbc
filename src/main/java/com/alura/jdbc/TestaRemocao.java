package com.alura.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestaRemocao {
    public static void main(String[] args) {
        try (Connection con = ConnectionFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("DELETE FROM loja_virtual.produto WHERE id > ?;")
        ) {
            statement.setInt(1, 2);
            statement.execute();
            System.out.println("Quantidade de linhas modificadas: " + statement.getUpdateCount());
        } catch (SQLException e) {
            System.out.println("Uma exceção foi lançada: ");
            System.out.println("Erro: " + e.getMessage());
        }
    }
}
