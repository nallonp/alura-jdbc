package com.alura.jdbc.dao;

import com.alura.jdbc.modelo.Categoria;
import com.alura.jdbc.modelo.Produto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {
    private final Connection connection;

    /*  Receber a conexão dessa forma no construtor permite que outro objeto que
     * use os métodos desse objeto faça um controle da transação através do
     * connection.setAutoCommit(false);
     * */
    public ProdutoDAO(Connection connection) {
        this.connection = connection;
    }

    public void salvarProduto(Produto produto) throws SQLException {
        System.out.println("Executando >>salvarProduto()@ProdutoDAO");
        String sql = ("INSERT INTO loja_virtual.produto (nome, descricao) VALUES (?, ?)");
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, produto.getNome());
            statement.setString(2, produto.getDescricao());
            statement.execute();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                while (resultSet.next()) produto.setId(resultSet.getInt(1));
            }
        }
    }

    public List<Produto> listar() throws SQLException {
        System.out.println("Executando >>lsitar()@ProdutoDAO");
        List<Produto> produtos = new ArrayList<>();
        String sql = "SELECT id, nome, descricao FROM loja_virtual.produto";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                while (resultSet.next()) produtos.add(new Produto(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3)));
            }
        }
        return produtos;
    }

    public List<Produto> buscar(Categoria categoria) throws SQLException {
        System.out.println("Executando >>buscar()@ProdutoDAO");
        List<Produto> produtos = new ArrayList<>();
        String sql = "SELECT id, nome, descricao FROM loja_virtual.produto WHERE categoria_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, categoria.getId());
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                while (resultSet.next()) produtos.add(new Produto(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3)));
            }
        }
        return produtos;
    }
}
