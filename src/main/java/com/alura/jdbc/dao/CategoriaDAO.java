package com.alura.jdbc.dao;

import com.alura.jdbc.modelo.Categoria;
import com.alura.jdbc.modelo.Produto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAO {

    private final Connection connection;

    public CategoriaDAO(Connection connection) {
        this.connection = connection;
    }

    public List<Categoria> listar() throws SQLException {
        System.out.println("Executando >>listar()@CategoriaDAO");
        List<Categoria> categorias = new ArrayList<>();
        String sql = "SELECT id, nome FROM loja_virtual.categoria";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                while (resultSet.next()) {
                    categorias.add(new Categoria(
                            resultSet.getInt(1),
                            resultSet.getString(2)));
                }
            }
        }
        return categorias;
    }

    public List<Categoria> listarComProdutos() throws SQLException {
        System.out.println("Executando >>listarComProdutos()@CategoriaDAO");
        Categoria ultima = null;
        List<Categoria> categorias = new ArrayList<>();
        String sql =
                "SELECT c.id, c.nome, p.id, p.nome,p.descricao " +
                        "FROM loja_virtual.categoria c " +
                        "INNER JOIN loja_virtual.produto p ON c.id = p.categoria_id";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                while (resultSet.next()) {
                    if (ultima == null || !ultima.getNome().equals(resultSet.getString(2))) {
                        Categoria categoria = new Categoria(resultSet.getInt(1), resultSet.getString(2));
                        ultima = categoria;
                        categorias.add(categoria);
                    }
                    Produto produto = new Produto(resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
                    ultima.adicionarProduto(produto);
                }
            }
        }
        return categorias;
    }
}
