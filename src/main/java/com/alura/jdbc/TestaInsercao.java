package com.alura.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercao {
    public static void main(String[] args) {
        try (Connection con = ConnectionFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("INSERT INTO loja_virtual.produto (nome, descricao) VALUES ('Mouse', 'Mouse sem fio')",
                     Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.execute();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                System.out.print("Produtos inserido: ");
                while (generatedKeys.next()) {
                    Integer id = generatedKeys.getInt(1);
                    System.out.println(id);
                }
            }
        } catch (SQLException e) {
            System.out.println("Uma exceção foi lançada: falha na abertura da conexão! ");
            System.out.println("Erro: " + e.getMessage());
        }
    }
}
